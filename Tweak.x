@interface BookmarksNavigationController : UINavigationController
-(id)initWithBookmarksDelegate:(id)delegate topBookmark:(id)bookmark skipOffset:(int)offset;
@end

%hook BookmarksNavigationController
-(id)initWithBookmarksDelegate:(id)delegate topBookmark:(id)bookmark skipOffset:(int)offset {
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  if([defaults objectForKey:@"selectedSpecialFolder"]){
    id realID=[defaults objectForKey:@"selectedBookmarkFolderID"];
    [defaults removeObjectForKey:@"selectedBookmarkFolderID"];
    self=%orig;
    NSArray* controllers=self.viewControllers;
    self.viewControllers=[controllers subarrayWithRange:NSMakeRange(1,controllers.count-1)];
    [defaults setObject:realID forKey:@"selectedBookmarkFolderID"];
  }
  else {self=%orig;}
  return self;
}
-(void)navigationController:(UINavigationController*)nav willShowViewController:(UIViewController*)sub animated:(BOOL)animated {
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  id current=[defaults objectForKey:@"selectedSpecialFolder"]?
   [defaults objectForKey:@"selectedBookmarkFolderID"]:nil;
  %orig;
  if(current){[defaults setObject:current forKey:@"selectedBookmarkFolderID"];}
}
-(void)viewDidAppear:(BOOL)animated {
  %orig;
  UINavigationBar* navbar=self.navigationBar;
  UISwipeGestureRecognizer* gesture;
  gesture=[[UISwipeGestureRecognizer alloc]
   initWithTarget:self action:@selector(SB_handleSwipeGesture:)];
  gesture.direction=UISwipeGestureRecognizerDirectionLeft;
  [navbar addGestureRecognizer:gesture];
  [gesture release];
  gesture=[[UISwipeGestureRecognizer alloc]
   initWithTarget:self action:@selector(SB_handleSwipeGesture:)];
  gesture.direction=UISwipeGestureRecognizerDirectionRight;
  [navbar addGestureRecognizer:gesture];
  [gesture release];
}
%new
-(void)SB_handleSwipeGesture:(UISwipeGestureRecognizer*)gesture {
  if(gesture.state!=UIGestureRecognizerStateEnded){return;}
  NSUserDefaults* defaults=[NSUserDefaults standardUserDefaults];
  NSString* current=[defaults objectForKey:@"selectedSpecialFolder"];
  NSString* const names[]={nil,@"history",@"readingList"};
  [defaults setObject:names[(([current isEqualToString:names[1]]?1:
   [current isEqualToString:names[2]]?2:0)+
   (gesture.direction==UISwipeGestureRecognizerDirectionLeft?1:-1)+3)%3]
   forKey:@"selectedSpecialFolder"];
  BookmarksNavigationController* tmp=[[%c(BookmarksNavigationController) alloc]
   initWithBookmarksDelegate:nil topBookmark:nil skipOffset:0];
  if(gesture.direction==UISwipeGestureRecognizerDirectionLeft){
    [self setViewControllers:tmp.viewControllers animated:YES];
  }
  else {
    self.viewControllers=[tmp.viewControllers arrayByAddingObject:self.topViewController];
    [self popViewControllerAnimated:YES];
  }
  [tmp release];
}
%end

%hook BookmarksTableViewController
-(BOOL)_inRootFolder {
  return NO;
}
%end

%hook BookmarkTextEntryTableViewCell
-(void)setUserInteractionEnabled:(BOOL)enabled {
  if(enabled){%orig;}
}
%end

#import <objc/runtime.h>
#import <WebKit/WebKit.h>

static Ivar $_vRootController;
static Ivar $_vBackItem,$_vForwardItem,$_vBackGesture,$_vForwardGesture;

@interface EXTERNAL
-(id)browserView;
-(void)hideBrowserPanel;
-(BOOL)_isSupportedInterfaceOrientation:(int)orientation;
-(void)_updateBackForward;
-(WebView*)webView;
@end

@interface BackForwardTableViewController
@property(assign) NSArray* backForwardList;
@property(assign) BOOL reversesListOrder;
-(id)delegate;
-(int)indexForIndexPath:(NSIndexPath*)path;
@end

%hook BrowserToolbar
-(void)_installGestureRecognizers {
  %orig;
  UILongPressGestureRecognizer* gesture;
  if(!(gesture=object_getIvar(self,$_vBackGesture))){
    object_setIvar(self,$_vBackGesture,gesture=[[UILongPressGestureRecognizer alloc]
     initWithTarget:self action:@selector(_backLongPressRecognized)]);
  }
  [[object_getIvar(self,$_vBackItem) view] addGestureRecognizer:gesture];
  if(!(gesture=object_getIvar(self,$_vForwardGesture))){
    object_setIvar(self,$_vForwardGesture,gesture=[[UILongPressGestureRecognizer alloc]
     initWithTarget:self action:@selector(_forwardLongPressRecognized)]);
  }
  [[object_getIvar(self,$_vForwardItem) view] addGestureRecognizer:gesture];
}
-(void)dealloc {
  [object_getIvar(self,$_vBackGesture) release];
  [object_getIvar(self,$_vForwardGesture) release];
  %orig;
}
%end

%hook BackForwardTableViewController
-(BOOL)_isSupportedInterfaceOrientation:(int)orientation {
  return [object_getIvar(self.delegate,$_vRootController)
   _isSupportedInterfaceOrientation:orientation];
}
%new
-(void)tableView:(UITableView*)view commitEditingStyle:(UITableViewCellEditingStyle)style forRowAtIndexPath:(NSIndexPath*)path {
  if(style==UITableViewCellEditingStyleDelete){
    WebView* wview=[[self.delegate browserView] webView];
    WebBackForwardList* list=wview.backForwardList;
    NSArray* array=self.backForwardList;
    [list removeItem:[array objectAtIndex:[self indexForIndexPath:path]]];
    if(!(self.backForwardList=self.reversesListOrder?
     [list backListWithLimit:array.count]:
     [list forwardListWithLimit:array.count]).count){
      [self.delegate hideBrowserPanel];
    }
    [wview.frameLoadDelegate _updateBackForward];
  }
}
%new
-(void)scrollViewDidScroll:(UIScrollView*)view {
  if(view.contentOffset.y<-80){[self.delegate hideBrowserPanel];}
}
%end

%ctor {
  $_vRootController=class_getInstanceVariable(%c(BrowserController),"_rootViewController");
  $_vBackItem=class_getInstanceVariable(%c(BrowserToolbar),"_backItem");
  $_vForwardItem=class_getInstanceVariable(%c(BrowserToolbar),"_forwardItem");
  $_vBackGesture=class_getInstanceVariable(%c(BrowserToolbar),"_backLongPressRecognizer");
  $_vForwardGesture=class_getInstanceVariable(%c(BrowserToolbar),"_forwardLongPressRecognizer");
  %init;
}
